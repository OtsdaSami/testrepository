コンパイル
	javac [.javaファイルのパス]
実行
	java [.classファイルのパス（拡張子は付けない）]

Javadoc生成
	javadoc -d doc -encoding UTF-8 -charset UTF-8 [.javaファイルのパス]


C:\pleiades\java\8\bin\javac.exe -encoding UTF-8 

C:\pleiades\java\8\bin\java.exe 

C:\pleiades\java\8\bin\javadoc.exe -encoding UTF-8 -charset UTF-8 

C:\pleiades\java\8\bin\java.exe -classpath .\kadai1  

実行方法の例：課題１（ファイル名はフルパスに置き換えること）
C:\pleiades\java\8\bin\java.exe kadai1.Kadai1 .\kadai1\KadaiText.txt Oh

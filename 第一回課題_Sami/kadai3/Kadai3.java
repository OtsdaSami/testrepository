package kadai3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 【クラスの概要】.
 * @author 【クラスの作成者名】
 */
public class Kadai3 {
	/**
	 * 【mainメソッドの概要】.
	 * <p>
	 * 【注釈とか処理の説明とか】
	 * </p>
	 *
	 * @param args[0]	【param（パラメータ）、引数の意味やどのような処理に用いるか】
	 * @param args[1]	【null値を許可するか、エラーを吐く条件があるのかを記述する】
	 * @throws java.io.IOException 【発生する可能性のある例外、発生条件などを記述】
	 */
	public static void main(String[] args) throws IOException {
		// 【変数の説明など】
		int		mainVariable1 = 0;
		// 【変数の説明など】
		String 	mainVariable2 = null;
		// 【変数の説明など】
		String 	mainVariable3 = null;
		// 【変数の説明など】
		String 	mainVariable4 = null;
		// 【変数の説明など】
		String	mainVariable6 = null;
		// 【変数の説明など】
		File 	mainVariable5 = null;


		// 【処理の説明など】
		try {
			mainVariable2 	= args[0];
			mainVariable3 	= args[1];
			mainVariable5	= new File(mainVariable2);
			// 【処理の説明など】
			mainVariable4 	= method1(mainVariable5);
			mainVariable1 	= method2(mainVariable4, mainVariable3);
			System.out.println(mainVariable3 + "=" + mainVariable1 + "件");
			// 【処理の説明など】
			mainVariable6 = String.valueOf(method3(mainVariable4.toCharArray(), mainVariable3.toCharArray()));
			System.out.println(mainVariable6);
		}catch(Exception e) {
			System.out.println("【例外（エラー）が発生した旨のテキスト】" + e);
		}
	}


	/**
	 * 【method1メソッドの概要】.
	 * <p>
	 * 【注釈とか処理の説明とか】
	 * </p>
	 *
	 * @param method1VariableA 【引数の役割などを記述】
	 * @return【戻り値の意味などを記述】
	 * @throws java.io.IOException 【発生する可能性のある例外、発生条件などを記述】
	 */
	public static String method1(File method1VariableA) throws IOException {
		BufferedReader method1Variable1 = null;
		// 【変数の説明など】
		int method1Variable2 = 0;
		// 【変数の説明など】
		StringBuffer method1Variable3 = new StringBuffer();

		try {
			// 【処理の説明・理由など】
			if(method1VariableA == null){
				return null;
			}
			// 【処理の説明など】
			method1Variable1 = new BufferedReader(new InputStreamReader(new FileInputStream(method1VariableA),"SJIS"));
			while ((method1Variable2 = method1Variable1.read()) != -1) {
				method1Variable3.append((char) method1Variable2);
			}
			// 【処理の説明・理由など】
			method1Variable1.close();
			return method1Variable3.toString();
		}catch(Exception e) {
			System.out.println(e);
			// 【処理の説明・理由など】
			method1Variable1.close();
			return null;
		}
	}


	/**
	 * 【method2メソッドの概要】.
	 * <p>
	 * 【注釈とか処理の説明とか】<br>
	 * 【<br>タグはJavadoc出力したときの改行コードになる】
	 * </p>
	 *
	 * @param method2VariableA 	【引数の役割などを記述】
	 * @param method2VariableB 		【引数の役割などを記述】
	 * @return 【戻り値の意味などを記述】
	 */
	public static int method2(String method2VariableA, String method2VariableB){
		// 【変数の説明など】
		int method2Variable1 	= 0;
		// 【変数の説明など】
		int method2Variable2 	= 0;
		// 【変数の説明など】
		int method2Variable3 	= 0;

		try{
			// 【処理の説明など】
			method2Variable2 = method2VariableA.length();
			method2Variable3 = method2VariableA.replaceAll(method2VariableB,"").length();
			method2Variable1 = (method2Variable2-method2Variable3)/method2VariableB.length();
		}catch(ArithmeticException e){
			method2Variable1 = 0;
		}
		return method2Variable1;
	}


	/**
	 * 【method3メソッドの概要】.
	 * <p>
	 * 【注釈とか処理の説明とか】
	 * </p>
	 * @param method3VariableA	【param（パラメータ）、引数の意味やどのような処理に用いるか】
	 * @param method3VariableB		【null値を許可するか、エラーを吐く条件があるのかを記述する】
	 * @return 【戻り値の意味などを記述】
	 */
	public static char[] method3(char[] method3VariableA, char[] method3VariableB){
		// 【変数の説明など】
		int method3Variable1 = 0;
		// 【変数の説明など】
		int method3Variable2 = 0;
		// 【変数の説明など】
		int method3Variable3 = 0;
		// 【変数の説明など】
		int method3Variable4 = 0;
		// 【変数の説明など】
		int method3Variable5 = 0;
		// 【変数の説明など】
		int method3Variable6 = 0;
		// 【変数の説明など】
		int method3Variable7 = 1;
		// 【変数の説明など】
		boolean method3Variable8 = false;
		// 【処理の説明など】

		int method4Variable1 = 0;
		for (char method4Variable2 : method3VariableA) {
			method4Variable1++;
		}
		method3Variable1 = method4Variable1;
		method4Variable1 = 0;
		for (char method4Variable2 : method3VariableB) {
			method4Variable1++;
		}
		method3Variable2 = method4Variable1;
		//【処理の説明・理由など】
		if( method3VariableB == null || method3Variable2 == 0){
					return null;
		}
		// 【変数の説明など】
		char[] method3Variable9 = new char[method3Variable1 * 2];
		// 【処理の説明など】
		while(method3Variable1 > method3Variable3){
			// 【処理の説明など】
			if(method3VariableA[method3Variable3] == '\r'){
				if(method3Variable8){
					method3Variable9[method3Variable5] = '\n';
					method3Variable5++;
					method3Variable8 =false;
					method3Variable6 = method3Variable5;
				}else{
					method3Variable5 = method3Variable6;
				}
				method3Variable3 += 2;
				method3Variable7++;
			}
			// 【処理の説明など】
			if(method3Variable6 == method3Variable5){
				method3Variable9[method3Variable5] = (char)('0' + method3Variable7);
				method3Variable5++;
				method3Variable9[method3Variable5] = ':';
				method3Variable5++;
			}
			// 【処理の説明など】
			if(method3Variable2 > method3Variable4 && method3VariableA[method3Variable3] == method3VariableB[method3Variable4]){
				method3Variable9[method3Variable5] = '[';
				method3Variable5++;
				while(method3Variable2 > method3Variable4){
					method3Variable9[method3Variable5] = method3VariableA[method3Variable3];
					method3Variable3++;
					method3Variable4++;
					method3Variable5++;
				}
				if(method3Variable2 <= method3Variable4){
					method3Variable8 = true ;
					method3Variable9[method3Variable5] = ']';
					method3Variable5++;
				}
			}else{
				method3Variable4 = 0;
				method3Variable9[method3Variable5] = method3VariableA[method3Variable3];
				method3Variable3++;
				method3Variable5++;
			}
		}
		char[] method5Variable1 = new char[method3Variable5];
		int method5Variable2 = 0;
		while(method3Variable5 > method5Variable2){
			method5Variable1[method5Variable2] = method3Variable9[method5Variable2];
			method5Variable2++;
		}
		method3Variable9 = method5Variable1;

		return method3Variable9;
	}
}
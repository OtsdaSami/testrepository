package kadai1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 【クラスの概要】.
 * @author 【クラスの作成者名】
 */
public class Kadai1 {
	/**
	 * 【mainメソッドの概要】.
	 * <p>
	 * 【注釈とか処理の説明とか】
	 * </p>
	 *
	 * @param args[0]	【param（パラメータ）、引数の意味やどのような処理に用いるか】
	 * @param args[1]	【null値を許可するか、エラーを吐く条件があるのかを記述する】
	 * @throws java.io.IOException 【発生する可能性のある例外、発生条件などを記述】
	 */
	public static void main(String[] args) throws IOException {
		// 【変数の説明など】
		int		mainVariable1 	= 0;
		// 【変数の説明など】
		String 	mainVariable2 	= null;
		// 【変数の説明など】
		String 	mainVariable3 	= null;
		// 【変数の説明など】
		String 	mainVariable4 	= null;
		// 【変数の説明など】
		File 	mainVariable5 	= null;

		// 【処理の説明など】
		try {
			mainVariable2 	= args[0];
			mainVariable3 	= args[1];
			mainVariable5	= new File(mainVariable2);
			// 【処理の説明など】
			mainVariable4 	= method1(mainVariable5);
			mainVariable1 	= method2(mainVariable4, mainVariable3);
			System.out.println(mainVariable3 + "=" + mainVariable1 + "件");
		}catch(Exception e) {
			System.out.println("【例外（エラー）が発生した旨のテキスト】" + e);
		}
	}


	/**
	 * 【method1メソッドの概要】.
	 * <p>
	 * 【注釈とか処理の説明とか】
	 * </p>
	 *
	 * @param method1VariableA 【引数の役割などを記述】
	 * @return【戻り値の意味などを記述】
	 * @throws java.io.IOException 【発生する可能性のある例外、発生条件などを記述】
	 */
	public static String method1(File method1VariableA) throws IOException {
		BufferedReader method1Variable1	= null;
		// 【変数の説明など】
		int method1Variable2 			= 0;
		// 【変数の説明など】
		StringBuffer method1Variable3	= new StringBuffer();

		try {
			// 【処理の説明・理由など】
			if(method1VariableA == null){
				return null;
			}
			// 【処理の説明など】
			method1Variable1 = new BufferedReader(new InputStreamReader(new FileInputStream(method1VariableA),"SJIS"));
			while ((method1Variable2 = method1Variable1.read()) != -1) {
				method1Variable3.append((char) method1Variable2);
			}
			// 【処理の説明・理由など】
			method1Variable1.close();
			return method1Variable3.toString();
		}catch(Exception e) {
			System.out.println(e);
			// 【処理の説明・理由など】
			method1Variable1.close();
			return null;
		}
	}


	/**
	 * 【method2メソッドの概要】.
	 * <p>
	 * 【注釈とか処理の説明とか】<br>
	 * 【<br>タグはJavadoc出力したときの改行コードになる】
	 * </p>
	 *
	 * @param method2VariableA 	【引数の役割などを記述】
	 * @param method2VariableB 		【引数の役割などを記述】
	 * @return 【戻り値の意味などを記述】
	 */
	public static int method2(String method2VariableA, String method2VariableB){
		// 【変数の説明など】
		int method2Variable1 	= 0;
		// 【変数の説明など】
		int method2Variable2 	= 0;
		// 【変数の説明など】
		int method2Variable3 	= 0;

		try{
			// 【処理の説明など】
			method2Variable2 = method2VariableA.length();
			method2Variable3 = method2VariableA.replaceAll(method2VariableB,"").length();
			method2Variable1 = (method2Variable2-method2Variable3)/method2VariableB.length();
		}catch(ArithmeticException e){
			method2Variable1 = 0;
		}
		return method2Variable1;
	}
}